﻿angular.module('App', [
        'gale-material', //ANGULAR-GALE & ANGULAR-GALE-MATERIALLIBRARY
        'app',
        'material-icons',
        'angularMoment', //ANGULAR MOMENT
        'facebook', //FACEBOOK SDK
        'nvd3ChartDirectives' //N3 CHART DIRECTIVES
    ])
    .run(function($location) {

        //REDIRECT TO MAIN HOME (ONLY WHEN NO HAVE PATH)
        /*var currentPath = $location.url();
        var boot = $location.path("public").search({
            path: currentPath
        });
        console.log(boot.url());
        $location.url(boot.url());*/

    })
    .config(function($ApiProvider) {
  
      //Configuración de la URL base
      $ApiProvider.setEndpoint('http://localhost/Ingenio/Streaming/API/v1/')

    })
    .config(function(FacebookProvider)
    {
        FacebookProvider.init('1820536568189350');
    })
    .config(function($IdentityProvider)
    {
        //Security Provider
        $IdentityProvider
            .enable() //Enable
            // FACEBOOK AUTHENTICATION
            //.setIssuerEndpoint("Security/Authorize") 
            .setLogInRoute("security/identity/social")
            .setWhiteListResolver(function(toState, current)
            {

                //Only Enable Access to Exception && Public State's
                if (toState.name.startsWith("exception.") ||
                    toState.name.startsWith("public."))
                {
                    return true;
                }

                //Restrict Other State's
                return false;

            });
    })
    .config(function($mdThemingProvider) {
        $mdThemingProvider.theme('default')
            .primaryPalette('indigo')
            .accentPalette('pink')
            .warnPalette('red');
    })
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('public', {
                url: "/public",
                abstract: true,
                // ---------------------------------------------
                // ONE-PAGE COLUMNS TEMPLATE
                // ---------------------------------------------
                templateUrl: "views/layouts/default.html",
                controller: "DefaultLayoutController"
            })
            .state('private', {
                url: "/private",
                abstract: true,
                // ---------------------------------------------
                // ONE-PAGE COLUMNS TEMPLATE
                // ---------------------------------------------
                templateUrl: "views/layouts/default.html",
                controller: "DefaultLayoutController"
            })
            .state('exception', {
                url: "/exception",
                abstract: true,
                // ---------------------------------------------
                // EXCEPTION TEMPLATE
                // ---------------------------------------------
                templateUrl: "views/layouts/exception.html",
                controller: "ExceptionLayoutController"
            });
        $urlRouterProvider.otherwise(function($injector, $location) {
            if ($location.path() !== "/") {
                var $state = $injector.get("$state");
                var $log = $injector.get("$log");

                $log.error("404", $location);
                $state.go("exception.error/404");
            }
        });
    })
    .config(function($logProvider, CONFIGURATION) {
        $logProvider.debugEnabled(CONFIGURATION.debugging || false);
    });
