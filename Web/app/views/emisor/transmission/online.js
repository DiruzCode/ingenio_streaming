angular.route('private.emisor/transmission/online', function(
    $scope,
    $state,
    $log,
    $q,
    $mdDialog,
    $mdConstant,
    $window,
    $interval
)
{   

    $scope.names = [
        {
            name : "Francisco Gutierrez"
        },
        {
            name : "Evelyn Escalona"
        },
        {
            name : "Ricardo Fuentes"
        },
        {
            name : "Kevin Zelada"
        },
        {
            name : "Juan Correa"
        },
        {
            name : "Marcelo Clas"
        },
        {
            name : "Sebastian Castillo"
        },
        {
            name : "Macarena Rebolledo"
        },
        {
            name : "Camila Fuenzalida"
        },
        {
            name : "Joaquin Gamucio"
        },
        {
            name : "Kenny Faundez"
        }

    ];
    $scope.model = {
        panelists: [],
        event : false
    };


    var previewMedia;

    $scope.testingMedia = function()
    {
        if (!previewMedia) {
          previewMedia = new Twilio.Conversations.LocalMedia();
          Twilio.Conversations.getUserMedia().then(
            function (mediaStream) {
              previewMedia.addStream(mediaStream);
              previewMedia.attach('#local-media');
            },
            function (error) {
              console.error('Unable to access local media', error);
              log('Unable to access Camera and Microphone');
            }
          );
        };
    };


    $scope.online = function(){
        $scope.testingMedia();  
        $scope.model.event = true;

        $scope.add_panelist = $interval(function() {
            if($scope.model.panelists.length > 10){
                $interval.cancel($scope.add_panelist);
                request_word_function();
            }
        

            $scope.model.panelists.push({
                name : $scope.names[Math.floor((Math.random() * 10) + 0)].name,
                picture : "bundles/mocks/avatar_"+Math.floor((Math.random() * 13) + 1)+".png",
                request_word : false
            });
        }, 4000);
    }

    $scope.offline = function(){
        $scope.model.event = true;
        $interval.cancel($scope.add_panelist);
    }

    var request_word_function = function(){
        $scope.model.panelists[Math.floor((Math.random() * 12) + 0)].request_word = true;
    }

    $scope.cancel = function()
    {
        $window.history.back();
    };

    $scope.showCalendar = function(ev, date)
    {
        $galeDatepickerDialog.show(ev,
        {
            selected: (date || new Date())
        }).then(function(selectedDate)
        {
            $scope.model.event.date = selectedDate;
        });
    };

});
