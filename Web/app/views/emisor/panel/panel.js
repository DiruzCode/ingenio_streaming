angular.route('private.emisor/panel/panel', function(
    $scope,
    $state,
    $log,
    $q,
    $mdDialog,
    $mdConstant,
    $galeDatepickerDialog,
    $window
)
{

    //---------------------------------------------------
    // Model
    $scope.model = {
        event: {
            date: new Date()
        },
        newcomment:{}
    };

    $scope.exampleData = [
             { key: "Panel", y: 70 },
             { key: "Seleccionados", y: 30 }
    ];

    $scope.details = function(){
        $state.go("app.events/details/details");
    };

    $scope.yFunction = function(){
        return function(d){
            return d.y;
        };
    };

    $scope.xFunction = function(){
        return function(d) {
            return d.key;
        };
    };


    //---------------------------------------------------
    // Action's

    $scope.showAddCallcenter = function()
    {
        $mdDialog.show(
            {
                controller: 'AddCallcenterDialogController',
                templateUrl: 'views/events/update/view/dialogs/addCallcenter.tpl.html',
                clickOutsideToClose: false,
                escapeToClose: true,
                focusOnOpen: true,
                fullscreen: true
            })
            .then(function(data)
            {
                //Update Data
            });
    };
    
    $scope.showUserEvents = function()
    {
        $mdDialog.show(
            {
                controller: 'EventUserDialogController',
                templateUrl: 'views/events/view/dialogs/eventUser.tpl.html',
                clickOutsideToClose: false,
                escapeToClose: true,
                focusOnOpen: true,
                fullscreen: true
            })
            .then(function(data)
            {
                //Update Data
            });
    };

    $scope.showOtherEvents = function()
    {
        $mdDialog.show(
            {
                controller: 'EventListDialogController',
                templateUrl: 'views/events/view/dialogs/eventList.tpl.html',
                clickOutsideToClose: false,
                escapeToClose: true,
                focusOnOpen: true,
                fullscreen: true
            })
            .then(function(data)
            {
                //Update Data
            });
    };
    
    $scope.cancel = function()
    {
        $window.history.back();
    };


    $scope.showCalendar = function(ev, date)
    {
        $galeDatepickerDialog.show(ev,
        {
            selected: (date || new Date())
        }).then(function(selectedDate)
        {
            $scope.model.event.date = selectedDate;
        });
    };

});
