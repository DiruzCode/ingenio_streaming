angular.route('private.callcenter/panel/panel', function(
    $scope,
    $state,
    $log,
    $q,
    $mdDialog,
    $mdConstant,
    $galeDatepickerDialog,
    $window
)
{

    //---------------------------------------------------
    // Model
    $scope.model = {
        event: {
            date: new Date()
        },
        newcomment:{}
    };

    $scope.exampleData = [
             { key: "Prospectos", y: 70 },
             { key: "Seleccionados", y: 30 }
    ];

    $scope.details = function(){
        $state.go("app.events/details/details");
    };

    $scope.yFunction = function(){
        return function(d){
            return d.y;
        };
    };

    $scope.xFunction = function(){
        return function(d) {
            return d.key;
        };
    };

    //---------------------------------------------------
    // Hour's For Time
    var times = [];
    var zeroDay = moment(0).startOf('day');
    for (var index = 0; index < 46; index++)
    {
        if (index > 0)
        {
            zeroDay.add(30, 'minutes');
        }

        times.push(
        {
            value: new Date(zeroDay.toDate()),
            label: zeroDay.format("HH:mm a").toUpperCase()
        });
    }
    $scope.times = times;


    //---------------------------------------------------
    // Action's


    $scope.showUserEvents = function()
    {
        $mdDialog.show(
            {
                controller: 'EventUserDialogController',
                templateUrl: 'views/events/view/dialogs/eventUser.tpl.html',
                clickOutsideToClose: false,
                escapeToClose: true,
                focusOnOpen: true,
                fullscreen: true
            })
            .then(function(data)
            {
                //Update Data
            });
    };
    
    $scope.showOtherEvents = function()
    {
        $mdDialog.show(
            {
                controller: 'EventListDialogController',
                templateUrl: 'views/events/view/dialogs/eventList.tpl.html',
                clickOutsideToClose: false,
                escapeToClose: true,
                focusOnOpen: true,
                fullscreen: true
            })
            .then(function(data)
            {
                //Update Data
            });
    };

    $scope.cancel = function()
    {
        $window.history.back();
    };


    $scope.showCalendar = function(ev, date)
    {
        $galeDatepickerDialog.show(ev,
        {
            selected: (date || new Date())
        }).then(function(selectedDate)
        {
            $scope.model.event.date = selectedDate;
        });
    };

});
