angular.route('private.admin/panel/panel', function(
    $scope,
    $state,
    $log,
    $q,
    $mdDialog,
    $mdConstant,
    $galeDatepickerDialog,
    $window,
    $interval
)
{

    //---------------------------------------------------
    // Model

    $scope.model = {
        index : 0,
        state : false,
        user: 1,
        when: "de Hoy",
        other_events : [
            {
                name : "Quien quiere ser millonario"
            },
            {
                name : "La tombola del destino"
            },
            {
                name : "La nueva revelacion del karma"
            },
            {
                name : "Ultima jugada mundial"
            },
            {
                name : "Aborto legal o ilegal"
            },
            {
                name : "Aborto legal o ilegal"
            }

        ],
        events : [
            {
                cantidad_eventos : 10,
                cantidad_dinero : 2500000,
                mas_ventas : "Ingeneria en minas un universo nuevo",
                emisor_mas_eventos : "Jose Jorquera Fierro"
            },
            {
                cantidad_eventos : 50,
                cantidad_dinero : 37000000,
                mas_ventas : "Teoria de la relatividad",
                emisor_mas_eventos : "Ernesto Acevedo"
            },
            {
                cantidad_eventos : 100,
                cantidad_dinero : 400000000,
                mas_ventas : "La nueva reforma tributaria",
                emisor_mas_eventos : "Joaquin Castillo Hernandez"
            },
            {
                cantidad_eventos : 520,
                cantidad_dinero : 3500000000,
                mas_ventas : "Quien quiere ser millonario",
                emisor_mas_eventos : "Francisco Rubio Solar"
            }

        ]
    };

    $scope.change_index = function(value, when){
        $scope.model.state = true;
        stop = $interval(function() {
            $scope.model.state = false;
            $interval.cancel(stop);
            $scope.model.when = when;
            $scope.model.index = value;
        }, 1000);
    };

    $scope.change_date = function(){
        $scope.model.state = true;
        stop = $interval(function() {
            $scope.model.state = false;
            $interval.cancel(stop);
            $scope.model.when = "desde la fecha "+ new Date($scope.model.date_init).toISOString().slice(0, 10) +" a la fecha "+ new Date($scope.model.date_end).toISOString().slice(0, 10);
            $scope.model.index = value;
        }, 1000);
    };


    $scope.showDetailsEvents = function(ev, item)
    {
        $mdDialog.show(
            {
                controller: 'EventInfoDetailsDialogController',
                templateUrl: 'views/admin/view/dialogs/eventInfoDetails.tpl.html',
                clickOutsideToClose: false,
                escapeToClose: true,
                focusOnOpen: true,
                fullscreen: true,
                locals: {
                   items: item
                 }

            })
            .then(function(data)
            {
                //Update Data
            });
    };

    //---------------------------------------------------
    // Hour's For Time
    var times = [];
    var zeroDay = moment(0).startOf('day');
    for (var index = 0; index < 46; index++)
    {
        if (index > 0)
        {
            zeroDay.add(30, 'minutes');
        }

        times.push(
        {
            value: new Date(zeroDay.toDate()),
            label: zeroDay.format("HH:mm a").toUpperCase()
        });
    }
    $scope.times = times;


    //---------------------------------------------------
    // Action's

    $scope.cancel = function()
    {
        $window.history.back();
    };


    $scope.showCalendar = function(ev, date)
    {
        $galeDatepickerDialog.show(ev,
        {
            selected: (date || new Date())
        }).then(function(selectedDate)
        {
            $scope.model.event.date = selectedDate;
        });
    };

});
