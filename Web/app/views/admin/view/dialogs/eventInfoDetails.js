angular.module('app.controllers')
    .controller('EventInfoDetailsDialogController', function(
        $scope,
        $log,
        $mdDialog,
        items
    )
    {
        //---------------------------------------------------
        // Model
        $scope.items = items;
        $scope.model = {
            event: {
                date: new Date()
            },
            newcomment:{}
        };


        $scope.exampleData = [
                 { key: "Prospectos", y: 70 },
                 { key: "Seleccionados", y: 30 }
         ];

         $scope.yFunction = function(){
            return function(d){
                return d.y;
            };
        };

        $scope.xFunction = function(){
            return function(d) {
                return d.key;
            };
        };

        $scope.model.data = [];

        //---------------------------------------------------
        // Action's
        $scope.cancel = function()
        {
            $mdDialog.cancel();
        };

    });
