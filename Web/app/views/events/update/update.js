angular.route('private.events/update/update/:token', function(
    $scope,
    $state,
    $log,
    $q,
    $mdDialog,
    $mdConstant,
    $galeDatepickerDialog,
    $window,
    $Api,
    $stateParams,
    $Identity
)
{

    $scope.model = {
        event: {
            primarysid : $Identity.getCurrent().primarysid,
            filtros : [],
            callcenters : [],
            tags: []
        },
        results:
        {
            items: []
        },
        tags:
        {
            electricChars: [
                $mdConstant.KEY_CODE.ENTER, $mdConstant.KEY_CODE.COMMA, $mdConstant.KEY_CODE.TAB
                //,$mdConstant.KEY_CODE.SPACE
            ]
        }
    };

    $Api.read("events/"+$stateParams.token,{})
    .success(function(data)
    {
        $scope.model.event = data.EVEN;
        $scope.model.event.filtros = data.filtros;
        $scope.model.event.callcenters = data.callcenters;
    })
    .finally(function()
    {
        
    });

    $log.debug($stateParams.token);
    var throwError = function(message)
    {
        $scope.onError(message);
    };

    $scope.showInvitePanel = function()
    {
        $mdDialog.show(
            {
                controller: 'InviteProspectsDialogController',
                templateUrl: 'views/events/update/view/dialogs/inviteProspects.tpl.html',
                clickOutsideToClose: false,
                escapeToClose: true,
                focusOnOpen: true,
                fullscreen: true,
                locals: {
                   event: $scope.model.event
                 }
            })
            .then(function(data)
            {
                //Update Data
            });
    };

    $scope.showFilterEvents = function(ev, item)
    {
        $mdDialog.show(
            {
                controller: 'FilterDialogController',
                templateUrl: 'views/events/create/view/dialogs/filter.tpl.html',
                clickOutsideToClose: false,
                escapeToClose: true,
                focusOnOpen: true,
                fullscreen: true
            })
            .then(function(data)
            {
                $log.debug(data);
                //Update Data
            });
    };

    $scope.showAddCallcenter = function()
    {
        $mdDialog.show(
            {
                controller: 'AddCallcenterDialogController',
                templateUrl: 'views/events/update/view/dialogs/addCallcenter.tpl.html',
                clickOutsideToClose: false,
                escapeToClose: true,
                focusOnOpen: true,
                fullscreen: true,
                locals: {
                   event: $scope.model.event
                 }
            })
            .then(function(data)
            {
                $log.debug(data);
                //Update Data
            });
    };
    
    $scope.save = function()
    {           
        $scope.updated = true; 
        $Api.update("events/"+$stateParams.token, $scope.model.event)
            .success(function(success)
            {
                $scope.updated = false;
                $log.debug(success);

            }).error(throwError);
    }


    var throwError = function(message)
    {   
        $log.error(message);
    };

    //---------------------------------------------------
    // Hour's For Time
    var times = [];
    var zeroDay = moment(0).startOf('day');
    for (var index = 0; index < 46; index++)
    {
        if (index > 0)
        {
            zeroDay.add(30, 'minutes');
        }

        times.push(
        {
            value: new Date(zeroDay.toDate()),
            label: zeroDay.format("HH:mm a").toUpperCase()
        });
    }
    $scope.times = times;
    //---------------------------------------------------

// Function's
    $scope.queryTags = function(query)
    {
        var deferred = $q.defer();

        $Api.kql("Filters/tags",
        {
            filters: [
            {
                property: "nombre",
                operator: "contains",
                value: query
            }]
        }).success(function(data)
        {
            deferred.resolve(data.items);
        });
        return deferred.promise;
    };

    $scope.setOrRegister = function(chip)
    {

        // If it is an object, it's already a known chip
        if (angular.isObject(chip))
        {
            return chip;
        }

        // Otherwise, create a new one
        return {
            token : null,
            nombre: chip
            
        };
    };
    //---------------------------------------------------
    // Action's

    $scope.cancel = function()
    {
        $window.history.back();
    };
});
