angular.module('app.controllers')
    .controller('AddCallcenterDialogController', function(
        $scope,
        $log,
        $mdDialog,
        $mdConstant,
        event,
        $q,
        $Api
    )
    {

        $scope.model = {
            event: {
                filtros : [],
                tags: [],
                callcenters: []
            }
        };

        $scope.model.event = event;

        $scope.model_modal = {
            token : $scope.model.event.Token,
            callcenters: []
        }


        $scope.addCallcenter = function(query){
            if(query){
                if(!findCallcenter(query.token)){
                    $scope.model_modal.callcenters.push(query);
                }
            }
        };

        $scope.deleteCallCenter = function(callcenter, index){
            $Api.delete("UserCallcenters", $scope.model_modal)
            .success(function(success)
            {

                $scope.model.event.callcenters.splice(index, 1);

            }).error(throwError);

        };


        $scope.queryTags = function(query)
        {
            var deferred = $q.defer();

            $Api.kql("Accounts/nombres",
            {
                filters: [
                {
                    property: "nombre",
                    operator: "contains",
                    value: query
                }]
            }).success(function(data)
            {
                deferred.resolve(data.items);
            });
            return deferred.promise;
        };

        //---------------------------------------------------
        // Action's
        $scope.save = function()
        {

            $Api.create("UserCallcenters", $scope.model_modal)
            .success(function(success)
            {

                $log.debug(success);

            }).error(throwError);
        };

        var throwError = function(message)
        {
            $log.error(message);
        };

        $scope.cancel = function()
        {
            $mdDialog.cancel();
        };

        var findCallcenter = function(token){
            for(var i = 0, len = $scope.model.event.callcenters.length; i < len; i++){
                if($scope.model.event.callcenters[i].token.includes(token)){
                    return true;
                }
            }

            return false;
        }

    });
