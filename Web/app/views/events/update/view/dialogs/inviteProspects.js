angular.module('app.controllers')
    .controller('InviteProspectsDialogController', function(
        $scope,
        $log,
        $mdDialog,
        $mdConstant,
        event,
        $q,
        $Api
    )
    {

        $scope.model = {
            event: {
                filtros : [],
                tags: [],
                callcenters: [],
                panel: [],
                sobrecupo: []
            }
        };

        $scope.model.event = event;

        $scope.model_modal = {
            token : $scope.model.event.Token,
            prospectos: []
        }


        $scope.addUserPanel = function(query){
            if(query){
                if(!findProspecto(query.token)){
                    $scope.model_modal.prospectos.push(query);
                }
            }
        }

        $scope.addProspecto = function(query){
            if(query){
                if(!findCallcenter(query.token)){
                    $scope.model_modal.prospectos.push(query);
                }
            }
        };

        $scope.deleteProspecto = function(callcenter, index){
            $Api.delete("UserProspectos", $scope.model_modal)
            .success(function(success)
            {
                $scope.model.event.prospectos.splice(index, 1);

            }).error(throwError);

        };


        $scope.queryTags = function(query)
        {
            var deferred = $q.defer();

            $Api.kql("Accounts/nombres",
            {
                filters: [
                {
                    property: "nombre",
                    operator: "contains",
                    value: query
                }]
            }).success(function(data)
            {
                deferred.resolve(data.items);
            });
            return deferred.promise;
        };

        var throwError = function(message)
        {
            $log.error(message);
        };

        $scope.cancel = function()
        {
            $mdDialog.cancel();
        };

        var findProspecto = function(token){
            for(var i = 0, len = $scope.model.model_modal.prospectos.length; i < len; i++){
                if($scope.model.model_modal.prospectos[i].token.includes(token)){
                    return true;
                }
            }

            return false;
        }

    });
