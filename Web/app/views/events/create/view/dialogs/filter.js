angular.module('app.controllers')
    .controller('FilterDialogController', function(
        $scope,
        $log,
        $mdDialog
    )
    {
        //---------------------------------------------------
        // Model
        $scope.model = {
            event: {
                date: new Date()
            },
            categories : ['Informatica', 'Leyes', 'Cocina', 'Medicina', 'Naturaleza', 'Farandula'],
            country : ['Chile', 'Argentina', 'Peru', 'Venezuela', 'Italia', 'Japon'],
            gender : ['Masculino', 'Femenino'],
            newcomment:{}
        };


        //---------------------------------------------------
        // Action's
        $scope.cancel = function()
        {
            $mdDialog.cancel();
        };

    });
