angular.route('private.events/create/step-1', function(
    $scope,
    $state,
    $log,
    $q,
    $Api,
    $mdDialog,
    $mdConstant,
    $galeDatepickerDialog,
    $window,
    $Identity
)
{

    $scope.model = {
        event: {
            primarysid : $Identity.getCurrent().primarysid,
            filtros : [],
            tags: []
        },
        results:
        {
            items: []
        },
        tags:
        {
            electricChars: [
                $mdConstant.KEY_CODE.ENTER, $mdConstant.KEY_CODE.COMMA, $mdConstant.KEY_CODE.TAB
                //,$mdConstant.KEY_CODE.SPACE
            ]
        }
    };

    $scope.showFilterEvents = function(ev, item)
    {
        $mdDialog.show(
            {
                controller: 'FilterDialogController',
                templateUrl: 'views/events/create/view/dialogs/filter.tpl.html',
                clickOutsideToClose: false,
                escapeToClose: true,
                focusOnOpen: true,
                fullscreen: true
            })
            .then(function(data)
            {
                //Update Data
            });
    };

    $scope.create = function(){
            $log.debug($scope.model);
            $scope.created = true;
            $Api.create("events", $scope.model.event)
            .success(function(success)
            {
                $state.go('private.events/update/update',{token : success.token});

            }).error(throwError);
    }

    $scope.calcule = function(){
        $scope.model.event.sobrecupo = Math.round($scope.model.event.cupo * 0.25);
        $scope.model.event.costo = $scope.model.event.cupo * 3000;
    }



    var throwError = function(message)
    {   
        $log.error(message);
        $scope.created = false;
        //$scope.onError(message);
    };

    //---------------------------------------------------
    // Hour's For Time
    var times = [];
    var zeroDay = moment(0).startOf('day');
    for (var index = 0; index < 46; index++)
    {
        if (index > 0)
        {
            zeroDay.add(30, 'minutes');
        }

        times.push(
        {
            value: new Date(zeroDay.toDate()),
            label: zeroDay.format("HH:mm a").toUpperCase()
        });
    }
    $scope.times = times;
    //---------------------------------------------------


        //---------------------------------------------------
    // Function's
    $scope.queryTags = function(query)
    {
        var deferred = $q.defer();

        $Api.kql("Filters/tags",
        {
            filters: [
            {
                property: "nombre",
                operator: "contains",
                value: query
            }]
        }).success(function(data)
        {
            deferred.resolve(data.items);
        });
        return deferred.promise;
    };

    $scope.setOrRegister = function(chip)
    {

        // If it is an object, it's already a known chip
        if (angular.isObject(chip))
        {
            return chip;
        }

        // Otherwise, create a new one
        return {
            token : null,
            nombre: chip
            
        };
    };

    //---------------------------------------------------
    // Action's

    $scope.cancel = function()
    {
        $window.history.back();
    };
});
