angular.module('app.controllers')
    .controller('EventUserDialogController', function(
        $scope,
        $log,
        $mdDialog
    )
    {
        //---------------------------------------------------
        // Model
        $scope.model = {
            event: {
                date: new Date()
            },
            newcomment:{}
        };

        //---------------------------------------------------
        // Action's
        $scope.cancel = function()
        {
            $mdDialog.cancel();
        };

    });
