angular.module('app.controllers')
    .controller('EventListDialogController', function(
        $scope,
        $log,
        $mdDialog,
        $state
    )
    {
        //---------------------------------------------------
        // Model
        $scope.myEvents = [
        {
          name : "Quien quiere ser millonario",
          date: "25-07-2016 12:00",
          panel: 35,
          postulantes: 50,
          seleccionados : 20,
          viwers: 0,
          callcenter: "Automatico"
        }, 
        {
          name : "La tombola del destino",
          date: "28-07-2016 12:00",
          panel: 50,
          postulantes: 60,
          seleccionados : 50,
          viwers: 10,
          callcenter: "Personal"
        },
        {
          name : "La nueva revelacion del karma",
          date: "05-08-2016 12:00",
          panel: 100,
          postulantes: 70,
          seleccionados : 70,
          viwers: 0,
          callcenter: "Personal"
        },
        {
          name : "Ultima jugada mundial",
          date: "25-08-2016 12:00",
          panel: 50,
          postulantes: 35,
          seleccionados : 20,
          viwers: 0,
          callcenter: "Automatico"
        },
        {
          name : "Aborto legal o ilegal",
          date: "29-08-2016 12:00",
          panel: 150,
          postulantes: 200,
          seleccionados : 100,
          viwers: 0,
          callcenter: "Automatico"
        }
    ];

        //---------------------------------------------------
        // Action's
        $scope.cancel = function()
        {
            $mdDialog.cancel();
        };

        $scope.update = function(){
          $scope.cancel();
          $state.go("app.events/update/update");
        };
        

    });
