angular.route('private.events/details/details', function(
    $scope,
    $state,
    $log,
    $q,
    $mdDialog,
    $mdConstant,
    $window
)
{

 //---------------------------------------------------
        // Model
    $scope.model = {
        event: {
            date: new Date()
        },
        panel : [
            {
                name : "Gerardo Sepulveda Gutierrez"
            },
            {
                name : "Esteban Jara Rebolledo"
            },
            {
                name : "Felipe Muñoz Escalona"
            },
            {
                name : "Claudia Jimenez Muñoz"
            },
            {
                name : "David Jorquera Muñoz"
            },
            {
                name : "Rossana Ossandon Castro"
            },
            {
                name : "Rosio del Carmen"
            },
            {
                name : "Marcelo Tapia Perez"
            },
            {
                name : "Evelyn San martin larenas"
            },
            {
                name : "Valentina Tapia Muñoz"
            },
            {
                name : "Jonathan Fuentealba pinto"
            }
        ],
        postulante : [
            {
                name : "Jose Miguel Muñoz"
            },
            {
                name : "Joaquin Gamucio Rebolledo"
            },
            {
                name : "Francisco Escalona Jimenez"
            },
            {
                name : "Pedro Rubio Jorquera"
            },
            {
                name : "Francisca Echeñique Perez"
            },
            {
                name : "Jorge Muñoz Castillo"
            }
        ],
        sobrecupo : [
            {
                name : "Ivan Gonzales Gonzales"
            }
        ],
        newcomment:{}
    };

     $scope.showUserEvents = function()
        {
            $mdDialog.show(
                {
                    controller: 'EventUserDialogController',
                    templateUrl: 'views/events/view/dialogs/eventUser.tpl.html',
                    clickOutsideToClose: false,
                    escapeToClose: true,
                    focusOnOpen: true,
                    fullscreen: true
                })
                .then(function(data)
                {
                    //Update Data
                });
        };

});
