angular.module('app.layouts').controller('DefaultLayoutController', function(
    $scope,
    $mdSidenav,
    $state,
    $log,
    $Configuration,
    $timeout,
    $Api,
    $location,
    $Identity
) {

    //------------------------------------------------------------------------------------
    // Model
    $scope.config = {
        application: $Configuration.get("application")
    };

    //------------------------------------------------------------------------------------
    // Bootstrapping : Get the Menu 
    $scope.config.menu = [{
        "name": "Dashboard",
        "token": "7414aa4e-c330-49b5-85dd-d065b57dd08d",
        "open": true,
        "items": [{
            "name": "Inicio",
            "icon": "action:home",
            "url": "public/home/index"
        }]
    }, {
        "name": "Paneles de administracion",
        "token": "7414aa4e-c330-49b5-85dd-d065b57dd08d",
        "open": true,
        "items": [{
            "name": "Panel de administrador",
            "icon": "action:view_list",
            "url": "private/admin/panel/panel"
        },{
            "name": "Panel de emisor",
            "icon": "action:view_list",
            "url": "private/emisor/panel/panel"
        },{
            "name": "Panel de callcenter",
            "icon": "action:view_list",
            "url": "private/callcenter/panel/panel"
        }]
    }, {
        "name": "Eventos",
        "token": "7414aa4e-c330-49b5-85dd-d065b57dd08d",
        "open": true,
        "items": [{
            "name": "Crear evento",
            "icon": "action:event",
            "url": "private/events/create/step-1"
        },{
            "name": "Transmision",
            "icon": "action:perm_camera_mic",
            "url": "private/emisor/transmission/online"
        }]
    }];

    //------------------------------------------------------------------------------------
    // Layout Actions
    $scope.link = function(url) {
        $timeout(function() {
            $location.url(url);
        }, 300);
        $mdSidenav('layout-nav-left').close();
    };

    $scope.toogleMenu = function(side) {
        $mdSidenav(side).toggle();
    };

    $scope.toggleSection = function(section) {
        section.open = !section.open;
    };

    $scope.navigateTo = function(item) {
        $scope.link(item.url);
    };

    $scope.logout = function() {
        $Identity.logOut();
    };
});
