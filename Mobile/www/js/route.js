angular.module('App')
.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "views/layouts/layout.html",
      controller: "DefaultLayoutController"
    })
    .state('login', {
      url: '/login',
      templateUrl: 'views/security/identity/login.html',
      controller: 'LoginController'
    })
    .state('app.home', {
      url: '/home',
      templateUrl: 'views/home/views/home.html',
      controller: 'HomeController'
    })
})
