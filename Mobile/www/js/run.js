angular.module('App')
.run(function($ionicPlatform, $state) {
  $ionicPlatform.ready(function() {
    if (window.StatusBar) {
      StatusBar.styleLightContent();
    }
    $state.go('login');
  });
});
