angular.module('App')
    .service("$Api", ['$http',
        function($http) {
            this.GET = function(url) {
                return $http.get('http://52.67.53.239/api/v1/' + url).success(function(data) {
                    return data;
                });
            };
        }
    ]);
