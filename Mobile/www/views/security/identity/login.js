app.controller('LoginController', [
  '$scope',
  '$state',
  '$log',
  '$cordovaSplashscreen',
  '$Api',
  function($scope, $state, $log, $cordovaSplashscreen, $Api) {
      $scope.dashboard = true;
      $scope.legend = "Registrando";
      $scope.title = "Usuario";

      $scope.user = {
          user_identity : ""
      }

      ionic.Platform.ready(function()
      {
          if (ionic.Platform.isWebView())
          {
              $cordovaSplashscreen.hide();
          }
      });


      ionic.Platform.ready(function()
      {
          if (ionic.Platform.isWebView())
          {
              $cordovaSplashscreen.hide();
          }
      });



      $scope.login = function()
      {
          $scope.dashboard = false;
          $scope.legend = "Esto tomara unos segundos";
          $scope.title = "Registrando Usuario";

          $Api.GET('server-token/'+$scope.user.user_identity.replace(/\s+/g, '').toLowerCase()).then(function(result) {

              localStorage.setItem("access_token", result.data.token);
              localStorage.setItem("identity", $scope.user.user_identity.replace(/\s+/g, '').toLowerCase());
              $state.go('app.home');
          });
      };

}]);
