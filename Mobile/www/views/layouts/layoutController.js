app.controller('DefaultLayoutController', [
  '$scope',
  '$state',
  '$http',
  function($scope, $state, $http) {
      $scope.config = {
        menu:  [{
          route: "app.home",
          icon: "ion-ios-home",
          label: "Inicio",
          active: true
        }, {
          route: "app.route-pending",
          icon: "ion-chevron-right",
          label: "Eventos Pasados"
        },{
          route: "app.route-finish",
          icon: "ion-chevron-right",
          label: "Proximos Eventos"
        }, {
          route: "app.activity-create",
          icon: "ion-chevron-right",
          label: "Crea un evento"
        }]
      };

      $scope.navigateTo = function(item) {
        angular.forEach($scope.config.menu, function(item) {
          item.active = false;
        });
         item.active = true;
        //$state.go(item.route);

      };
  }
])
