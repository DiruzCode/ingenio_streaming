app.controller('HomeController', [
  '$scope',
  '$state',
  '$Api',
  '$log',
  '$q',
  function($scope, $state, $Api, $log, $q) {
      $scope.conversationsClient = null;
      $scope.user_identity_invite = "";
      $scope.activeConversation = null;
      $scope.previewMedia = false;
      $scope.accessManager = null
      $scope.dashboard = false;
      var invite = 0;

      $scope.user = {
          user_identity : "",
          register : false,
          user_identity_invite : ""
      }
      $scope.legend = "";
      $scope.title = "Esperando ingreso al evento";

      // Check for WebRTC
      if (!navigator.webkitGetUserMedia && !navigator.mozGetUserMedia) {
          alert('WebRTC is not available in your browser.');
      }


      //GET TOKEN
      var init = function(){
          var accessManager = new Twilio.AccessManager(localStorage.getItem("access_token"));
          $scope.conversationsClient = new Twilio.Conversations.Client(accessManager);
          $scope.conversationsClient.listen().then(clientConnected, function (error) {
              $log.debug('Could not connect to Twilio: ' + error.message);
          });
      };

      /*$scope.actionConnect_twillio = function(){
          $scope.dashboard = false;
          $scope.legend = "Esto tomara unos segundos";
          $scope.title = "Registrando Usuario";

          $Api.GET('server-token/'+$scope.user.user_identity).then(function(result) {
              $scope.accessManager = new Twilio.AccessManager(result.data.token);
              if($scope.accessManager){
                  $scope.user.register = true;
                  $scope.dashboard = true;

                  $scope.conversationsClient = new Twilio.Conversations.Client($scope.accessManager);
                  $scope.conversationsClient.listen().then(clientConnected, function (error) {
                      $log.debug('Could not connect to Twilio: ' + error.message);
                  });
              }
          })
      };*/


      // Successfully connected!
      var clientConnected = function() {

          $log.debug("Connected to Twilio. Listening for incoming Invites as '" + $scope.conversationsClient.identity + "'");
          $scope.conversationsClient.on('invite', function (invite) {
              $scope.dashboard = true;
              $log.debug('Incoming invite from: ' + invite.from);
              invite.accept().then(conversationStarted);
          });
      }

      /*$scope.actionSendInvite = function(){
          $log.debug($scope.user.user_identity_invite);
          if ($scope.activeConversation) {
              // Add a participant
              $scope.activeConversation.invite($scope.user.user_identity_invite);
          } else {
              // Create a conversation
              var options = {};
              if ($scope.previewMedia) {
                  options.localMedia = $scope.previewMedia;
              }
              $scope.conversationsClient.inviteToConversation($scope.user.user_identity_invite, options).then(conversationStarted, function (error) {
                  $log.debug('Unable to create conversation');
                  $log.error('Unable to create conversation', error);
              });
          }
      }*/

      // Conversation is live
      function conversationStarted(conversation) {
          $log.debug('In an active Conversation');
          var count = 0;
          $scope.activeConversation = conversation;
           conversation.localMedia.attach('#local-media');
          // Draw local video, if not already previewing
          /*if (!$scope.previewMedia) {
              conversation.localMedia.attach('#local-media');
          }*/

          // When a participant joins, draw their video on screen
          conversation.on('participantConnected', function (participant) {
              if(participant.identity !== 'evento'){
                  participant.media.attach('#local-media');
              }else{
                  participant.media.attach('#remote-media');
              }
              count++;
              $log.debug("Participant '" + participant.identity + "' connected : Count : " + count);
          });

          /* 2. We can attach a Participant's Audio and Video Tracks to individual
            <audio> and <video> elements. */
          // When a participant disconnects, note in log
          conversation.on('participantDisconnected', function (participant) {
              $log.debug("Participant '" + participant.identity + "' disconnected");
          });

          // When the conversation ends, stop capturing local video
          conversation.on('disconnected', function (conversation) {
              $log.debug("Connected to Twilio. Listening for incoming Invites as '" + $scope.conversationsClient.identity + "'");
              conversation.localMedia.stop();
              conversation.disconnect();
              $scope.activeConversation = null;
          });
      }

      init();
}]);
