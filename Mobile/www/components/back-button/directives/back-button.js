'use strict';
app.directive('backButton', function(){
    return {
      restrict: 'A',
      scope: true,
      template: '<div class="buttons-toggle left-buttons"><button class="button button-icon icon ion-ios-arrow-back" ng-click="back()"></button></div>',
      controller: function($scope, $element, $rootScope, $state){
        $scope.back = function() {
            var length = $rootScope.aryStateHistory.length;
            if(length > 1){
                $state.go($rootScope.aryStateHistory[length - 1].fromState, $rootScope.aryStateHistory[length - 1].fromParams);
                $rootScope.aryStateHistory.splice(length - 1);
            }else{
                $state.go($rootScope.aryStateHistory[0].fromState, $rootScope.aryStateHistory[0].fromParams);
                $rootScope.aryStateHistory = [];
            }

        }
      }
    }
});
