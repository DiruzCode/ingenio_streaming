Proyecto Streaming Ingenio
==========

Herramientas de desarrollo
--------------------

+ Visual Studio 2015
+ Sql server 2016


Frameworks de desarrollo
--------------------

+ [Angular Gale](http://angular-gale.azurewebsites.net/)

+ [GaleFramework .Net](http://gale.azurewebsites.net/)

+ [ionic-AngularGale](https://github.com/dmunozgaete/angular-gale-starter-ionic)

Libreria de streaming de desarrollo
--------------------

+[Twilio C#](https://github.com/TwilioDevEd/video-quickstart-csharp)

+[Twilio Gestor de streaming](https://www.twilio.com)