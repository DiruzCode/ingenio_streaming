﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Twilio.Auth;

namespace API.Endpoints.Security.Services.Events
{
    /// <summary>
    /// Retrieve a Target User
    /// </summary>
    public class GetTwillioValidation : Gale.REST.Http.HttpReadActionResult<String>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="token_event"></param>
        /// <param name="token_user"></param>
        public GetTwillioValidation(String token_event, String token_user) : base(token_event) { }

        /// <summary>
        /// Async Process
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            var accountSid = System.Configuration.ConfigurationManager.AppSettings["TwilioAccountSid"];
            var apiKey = System.Configuration.ConfigurationManager.AppSettings["TwilioApiKey"];
            var apiSecret = System.Configuration.ConfigurationManager.AppSettings["TwilioApiSecret"];
            var videoConfigSid = System.Configuration.ConfigurationManager.AppSettings["TwilioConfigurationSid"];

            // Create a random identity for the client
            var identity = this.Model;

            // Create an Access Token generator
            var AccessTokenTwillio = new AccessToken(accountSid, apiKey, apiSecret);
            AccessTokenTwillio.Identity = identity;

            // Create a video grant for this token
            var grant = new VideoGrant();
            grant.ConfigurationProfileSid = videoConfigSid;
            AccessTokenTwillio.AddGrant(grant);
            string tokensend = AccessTokenTwillio.ToJWT();

            //----------------------------------------------------------------------------------------------------
            //Create Response
            var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
            {
                Content = new ObjectContent<Object>(
                    tokensend,
                    System.Web.Http.GlobalConfiguration.Configuration.Formatters.KqlFormatter()
                )
            };

            //Return Task
            return Task.FromResult(response);
            //----------------------------------------------------------------------------------------------------
        }
    }
}