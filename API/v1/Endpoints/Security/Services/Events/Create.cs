﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace API.Endpoints.Security.Services.Events
{
    /// <summary>
    /// Add User to DB
    /// </summary>
    public class Create : Gale.REST.Http.HttpCreateActionResult<Models.Events.NuevoEvento>
    {
        private string _host;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="user">New User Information</param>
        /// <param name="host">Application URL</param>
        public Create(Models.Events.NuevoEvento user, string host)
            : base(user)
        {
            this._host = host;
        }

        /// <summary>
        /// Async Process
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            //------------------------------------------------------------------------------------------------------
            // GUARD EXCEPTIONS
            Gale.Exception.RestException.Guard(() => Model == null, "BODY_EMPTY", API.Errors.ResourceManager);
            Gale.Exception.RestException.Guard(() => String.IsNullOrEmpty(Model.nombre), "NAME_EMPTY", API.Errors.ResourceManager);
            Gale.Exception.RestException.Guard(() => String.IsNullOrEmpty(Model.descripcion), "DESCRIPTION_EMPTY", API.Errors.ResourceManager);
            //------------------------------------------------------------------------------------------------------

            System.Guid TOKEN_EVENTO = System.Guid.NewGuid();
            //------------------------------------------------------------------------------------------------------
            // DB Execution
            using (Gale.Db.DataService svc = new Gale.Db.DataService("PA_PROD_INS_Evento"))
            {
                svc.Parameters.Add("USUA_Token", "6D32EB7E-3225-45CD-B54E-7065F87FD37D");
                svc.Parameters.Add("ARCH_Token", "BF52750B-12FF-49AE-B593-272091B30A29");
                svc.Parameters.Add("EVEN_Token", TOKEN_EVENTO);
                svc.Parameters.Add("EVEN_Nombre", Model.nombre);
                svc.Parameters.Add("EVEN_Descripcion", Model.descripcion);
                svc.Parameters.Add("EVEN_Tipo", Model.tipo);
                svc.Parameters.Add("EVEN_Cupo", Model.cupo);
                svc.Parameters.Add("EVEN_Activo", Model.activo);
                svc.Parameters.Add("EVEN_FechaInicio", Model.fechaInicio);
                svc.Parameters.Add("EVEN_FechaTermino", Model.fechaTermino);

                if (this.Model.filtros.Count > 0)
                {
                    //svc.Parameters.Add("Filtros", Model.filtros);
                    svc.AddTableType<Models.Events.Filtros>("T_Filtros", Model.filtros);
                    //Add("FILTROS", String.Join(",", this.Model.filtros));
                }

                try
                {
                    this.ExecuteAction(svc);
                }
                catch (Gale.Exception.SqlClient.CustomDatabaseException ex)
                {
                    throw new Gale.Exception.RestException(System.Net.HttpStatusCode.BadRequest, ex.Message, null);
                }
            }
            //------------------------------------------------------------------------------------------------------
            HttpResponseMessage response = new HttpResponseMessage(System.Net.HttpStatusCode.Created)
            {
                Content = new ObjectContent<Object>(
                    new
                    {
                        token = TOKEN_EVENTO
                    },
                    System.Web.Http.GlobalConfiguration.Configuration.Formatters.JsonFormatter
                  )
            };
            return Task.FromResult(response);
        }
    }
}