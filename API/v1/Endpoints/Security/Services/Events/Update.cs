﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Net.Http;

namespace API.Endpoints.Security.Services.Events
{
    /// <summary>
    /// Update User in DB
    /// </summary>
    public class Update : Gale.REST.Http.HttpUpdateActionResult<Models.Events.ActualizarEvento>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="token">User Token</param>
        /// <param name="user">Target Model</param>
        public Update(string token, Models.Events.ActualizarEvento user) : base(token, user) { }

       /// <summary>
       ///  Update User
       /// </summary>
       /// <param name="token"></param>
       /// <param name="cancellationToken"></param>
       /// <returns></returns>
        public override Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(string token, System.Threading.CancellationToken cancellationToken)
        {
            //------------------------------------------------------------------------------------------------------
            // GUARD EXCEPTIONS
            Gale.Exception.RestException.Guard(() => Model == null, "BODY_EMPTY", API.Errors.ResourceManager);
            Gale.Exception.RestException.Guard(() => Model.nombre == String.Empty, "NAME_EMPTY", API.Errors.ResourceManager);
            Gale.Exception.RestException.Guard(() => Model.descripcion == String.Empty, "DESCRIPTION_EMPTY", API.Errors.ResourceManager);
            //------------------------------------------------------------------------------------------------------

            //------------------------------------------------------------------------------------------------------
            // DB Execution
            using (Gale.Db.DataService svc = new Gale.Db.DataService("PA_PROD_ACT_Evento"))
            {

                svc.Parameters.Add("EVEN_Token", token);
                svc.Parameters.Add("EVEN_Nombre", Model.nombre);
                svc.Parameters.Add("EVEN_Descripcion", Model.descripcion);
                svc.Parameters.Add("ARCH_Token", "D6A3C712-CB81-48DE-BCA8-40C902986959");
                svc.Parameters.Add("EVEN_Tipo", Model.tipo);
                svc.Parameters.Add("EVEN_Activo", Model.activo);

                if (this.Model.filtros.Count > 0)
                {
                    //svc.Parameters.Add("Filtros", Model.filtros);
                    svc.AddTableType<Models.Events.Filtros>("T_Filtros", Model.filtros);
                    //Add("FILTROS", String.Join(",", this.Model.filtros));

                }
                this.ExecuteAction(svc);
            }
            //------------------------------------------------------------------------------------------------------

            HttpResponseMessage response = new HttpResponseMessage(System.Net.HttpStatusCode.PartialContent);

            return Task.FromResult(response);
        }
    }
}