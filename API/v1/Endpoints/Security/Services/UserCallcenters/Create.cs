﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace API.Endpoints.Security.Services.UserCallcenters
{
    /// <summary>
    /// Add User to DB
    /// </summary>
    public class Create : Gale.REST.Http.HttpCreateActionResult<Models.UserCallcenters.NuevosCallCenters>
    {
        private string _host;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="user">New User Information</param>
        /// <param name="host">Application URL</param>
        public Create(Models.UserCallcenters.NuevosCallCenters user, string host)
            : base(user)
        {
            this._host = host;
        }

        /// <summary>
        /// Async Process
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            //------------------------------------------------------------------------------------------------------
            // GUARD EXCEPTIONS
            Gale.Exception.RestException.Guard(() => Model == null, "BODY_EMPTY", API.Errors.ResourceManager);

            using (Gale.Db.DataService svc = new Gale.Db.DataService("PA_PROD_INS_UsuariosCallcenter"))
            {
                svc.Parameters.Add("EVEN_Token", Model.token);

                if (this.Model.callcenters.Count > 0)
                {
                    svc.AddTableType<Models.UserCallcenters.Entidades>("T_Entidades", Model.callcenters);
                }

                try
                {
                    this.ExecuteAction(svc);
                }
                catch (Gale.Exception.SqlClient.CustomDatabaseException ex)
                {
                    throw new Gale.Exception.RestException(System.Net.HttpStatusCode.BadRequest, ex.Message, null);
                }
            }
            HttpResponseMessage response = new HttpResponseMessage(System.Net.HttpStatusCode.Created);
            return Task.FromResult(response);
        }
    }
}