﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace API.Endpoints.Security.Services.UserEvents
{
    /// <summary>
    /// Add User to DB
    /// </summary>
    public class Create : Gale.REST.Http.HttpCreateActionResult<Models.UserEvents.NewUserEvent>
    {
        private string _host;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="user">New User Information</param>
        /// <param name="host">Application URL</param>
        public Create(Models.UserEvents.NewUserEvent user, string host)
            : base(user)
        {
            this._host = host;
        }

        /// <summary>
        /// Async Process
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            //------------------------------------------------------------------------------------------------------
            // GUARD EXCEPTIONS
            Gale.Exception.RestException.Guard(() => Model == null, "BODY_EMPTY", API.Errors.ResourceManager);

            //------------------------------------------------------------------------------------------------------
            // DB Execution
            using (Gale.Db.DataService svc = new Gale.Db.DataService("PA_PROD_INS_UsuarioEvento"))
            {
                svc.Parameters.Add("EVEN_Token", Model.USEV_EVEN_Token);
                svc.Parameters.Add("USUA_Token", Model.USEV_USUA_Token);
                try
                {
                    this.ExecuteAction(svc);
                }
                catch (Gale.Exception.SqlClient.CustomDatabaseException ex)
                {
                    throw new Gale.Exception.RestException(System.Net.HttpStatusCode.BadRequest, ex.Message, null);
                }
            }
            //------------------------------------------------------------------------------------------------------
            HttpResponseMessage response = new HttpResponseMessage(System.Net.HttpStatusCode.Created);
            return Task.FromResult(response);
        }
    }
}