﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Endpoints.Security.Services.Events
{
    /// <summary>
    /// Account API
    /// </summary>
    //[Gale.Security.Oauth.Jwt.Authorize]
    public class EventsController : Gale.REST.RestController
    {

        #region EVENT
        /// <summary>
        /// Retrieve Event's
        /// </summary>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.OK)]
        //[Gale.Security.Oauth.Jwt.Authorize(Roles = API.WebApiConfig.RootRoles)]
        public IHttpActionResult Get()
        {
            return new Gale.REST.Http.HttpQueryableActionResult<Models.Events.TB_PROD_Evento>(this.Request);
        }

        /// <summary>
        /// Retrieve Target Event Information
        /// </summary>
        /// <param name="id">Event Token</param>
        /// <returns></returns>
        [HttpGet]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.OK)]
       // [Gale.Security.Oauth.Jwt.Authorize(Roles = API.WebApiConfig.RootRoles)]
        public IHttpActionResult Get(String id)
        {
            //------------------------------------------------------------------------------------------------------
            // GUARD EXCEPTIONS
            Gale.Exception.RestException.Guard(() => !id.isGuid(), "ID_INVALID_GUID", API.Errors.ResourceManager);
            //------------------------------------------------------------------------------------------------------
            return new Services.Events.Get(id);
        }

        /// <summary>
        /// Get Token Access Twillio
        /// </summary>
        /// <param name="token_event">Event Token</param>
        /// <param name="token_user">Event Token</param>
        /// <returns></returns>
        [HttpGet]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.OK)]
        // [Gale.Security.Oauth.Jwt.Authorize(Roles = API.WebApiConfig.RootRoles)]
        [HierarchicalRoute("{token_event}/{token_user}")]
        public IHttpActionResult GetTwillioValidation(String token_event, String token_user)
        {
            //------------------------------------------------------------------------------------------------------
            // GUARD EXCEPTIONS
            Gale.Exception.RestException.Guard(() => !token_event.isGuid(), "ID_INVALID_GUID", API.Errors.ResourceManager);
            Gale.Exception.RestException.Guard(() => !token_user.isGuid(), "ID_INVALID_GUID", API.Errors.ResourceManager);
            //------------------------------------------------------------------------------------------------------
            return new Services.Events.GetTwillioValidation(token_event, token_user);
        }


        /// <summary>
        /// Created an event in the system
        /// </summary>
        /// <param name="param">Event information</param>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.Created)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.BadRequest)]
        //[Gale.Security.Oauth.Jwt.Authorize(Roles = API.WebApiConfig.RootRoles)]
        public IHttpActionResult Post([FromBody]Models.Events.NuevoEvento param)
        {
            string host = this.Request.Headers.Referrer.ToString();
            return new Services.Events.Create(param, host);
        }

        /// <summary>
        /// Update the target Account
        /// </summary>
        /// <param name="id">Event Token</param>
        /// <param name="param">Account information</param>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.NoContent)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.BadRequest)]
        //[Gale.Security.Oauth.Jwt.Authorize]
        public IHttpActionResult Put([FromUri]String id, [FromBody]Models.Events.ActualizarEvento param)
        {

            return new Services.Events.Update(id, param);

        }

        #endregion

    }
}