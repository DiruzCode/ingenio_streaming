﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Endpoints.Security.Services.Filters
{
    /// <summary>
    /// Account API
    /// </summary>
    //[Gale.Security.Oauth.Jwt.Authorize]
    public class FiltersController : Gale.REST.RestController
    {

        #region FILTERS
        /// <summary>
        /// Retrieve Filter's
        /// </summary>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.OK)]
        //[Gale.Security.Oauth.Jwt.Authorize(Roles = API.WebApiConfig.RootRoles)]
        public IHttpActionResult Get()
        {
            return new Gale.REST.Http.HttpQueryableActionResult<Models.Filters.TB_PROD_Filtro>(this.Request);
        }

        /// <summary>
        /// Retrieve Target Filter Information
        /// </summary>
        /// <param name="id">Filter ID</param>
        /// <returns></returns>
        [HttpGet]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.OK)]
       // [Gale.Security.Oauth.Jwt.Authorize(Roles = API.WebApiConfig.RootRoles)]
        public IHttpActionResult Get(String id)
        {
            return new Services.Filters.Get(id);
        }

        /// <summary>
        /// Created an filter in the system
        /// </summary>
        /// <param name="filter">Filter information</param>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.Created)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.BadRequest)]
        //[Gale.Security.Oauth.Jwt.Authorize(Roles = API.WebApiConfig.RootRoles)]
        public IHttpActionResult Post([FromBody]Models.Filters.NuevoFiltro filter)
        {
            string host = this.Request.Headers.Referrer.ToString();
            return new Services.Filters.Create(filter, host);
        }

        [HttpGet]
        [HierarchicalRoute("tags")]
        [Swashbuckle.Swagger.Annotations.QueryableEndpoint(typeof(Models.Filters.BuscarFiltro))]
        public IHttpActionResult Tags()
        {
            //return new Services.Filters.Tags(nombre);
            return new Gale.REST.Http.HttpQueryableActionResult<Models.Filters.BuscarFiltro>(this.Request);

        }

        #endregion

    }
}