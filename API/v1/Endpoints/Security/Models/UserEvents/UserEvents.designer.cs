﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace API.Endpoints.Security.Models.UserEvents
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="SD_IngenioStreaming")]
	public partial class UserEventsDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertTB_PROD_UsuarioEvento(TB_PROD_UsuarioEvento instance);
    partial void UpdateTB_PROD_UsuarioEvento(TB_PROD_UsuarioEvento instance);
    partial void DeleteTB_PROD_UsuarioEvento(TB_PROD_UsuarioEvento instance);
    #endregion
		
		public UserEventsDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["SD_IngenioStreamingConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public UserEventsDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public UserEventsDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public UserEventsDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public UserEventsDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<TB_PROD_UsuarioEvento> TB_PROD_UsuarioEventos
		{
			get
			{
				return this.GetTable<TB_PROD_UsuarioEvento>();
			}
		}
		
		public System.Data.Linq.Table<NewUserEvent> NewUserEvents
		{
			get
			{
				return this.GetTable<NewUserEvent>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TB_PROD_UsuarioEvento")]
	public partial class TB_PROD_UsuarioEvento : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _USEV_Codigo;
		
		private int _USEV_EVEN_Codigo;
		
		private int _USEV_USUA_Codigo;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnUSEV_CodigoChanging(int value);
    partial void OnUSEV_CodigoChanged();
    partial void OnUSEV_EVEN_CodigoChanging(int value);
    partial void OnUSEV_EVEN_CodigoChanged();
    partial void OnUSEV_USUA_CodigoChanging(int value);
    partial void OnUSEV_USUA_CodigoChanged();
    #endregion
		
		public TB_PROD_UsuarioEvento()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_USEV_Codigo", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int USEV_Codigo
		{
			get
			{
				return this._USEV_Codigo;
			}
			set
			{
				if ((this._USEV_Codigo != value))
				{
					this.OnUSEV_CodigoChanging(value);
					this.SendPropertyChanging();
					this._USEV_Codigo = value;
					this.SendPropertyChanged("USEV_Codigo");
					this.OnUSEV_CodigoChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_USEV_EVEN_Codigo", DbType="Int NOT NULL")]
		public int USEV_EVEN_Codigo
		{
			get
			{
				return this._USEV_EVEN_Codigo;
			}
			set
			{
				if ((this._USEV_EVEN_Codigo != value))
				{
					this.OnUSEV_EVEN_CodigoChanging(value);
					this.SendPropertyChanging();
					this._USEV_EVEN_Codigo = value;
					this.SendPropertyChanged("USEV_EVEN_Codigo");
					this.OnUSEV_EVEN_CodigoChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_USEV_USUA_Codigo", DbType="Int NOT NULL")]
		public int USEV_USUA_Codigo
		{
			get
			{
				return this._USEV_USUA_Codigo;
			}
			set
			{
				if ((this._USEV_USUA_Codigo != value))
				{
					this.OnUSEV_USUA_CodigoChanging(value);
					this.SendPropertyChanging();
					this._USEV_USUA_Codigo = value;
					this.SendPropertyChanged("USEV_USUA_Codigo");
					this.OnUSEV_USUA_CodigoChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TB_PROD_UsuarioEvento")]
	public partial class NewUserEvent
	{
		
		private int _USEV_EVEN_Codigo;
		
		private int _USEV_USUA_Codigo;
		
		public NewUserEvent()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="USEV_EVEN_Codigo", Storage="_USEV_EVEN_Codigo", DbType="Int NOT NULL")]
		public int USEV_EVEN_Token
		{
			get
			{
				return this._USEV_EVEN_Codigo;
			}
			set
			{
				if ((this._USEV_EVEN_Codigo != value))
				{
					this._USEV_EVEN_Codigo = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="USEV_USUA_Codigo", Storage="_USEV_USUA_Codigo", DbType="Int NOT NULL")]
		public int USEV_USUA_Token
		{
			get
			{
				return this._USEV_USUA_Codigo;
			}
			set
			{
				if ((this._USEV_USUA_Codigo != value))
				{
					this._USEV_USUA_Codigo = value;
				}
			}
		}
	}
}
#pragma warning restore 1591
