﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace API.Endpoints.Security.Models.UserCallcenters
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="SD_IngenioStreaming")]
	public partial class UserCallcenterDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertBuscarUsuario(BuscarUsuario instance);
    partial void UpdateBuscarUsuario(BuscarUsuario instance);
    partial void DeleteBuscarUsuario(BuscarUsuario instance);
    #endregion
		
		public UserCallcenterDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["SD_IngenioStreamingConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public UserCallcenterDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public UserCallcenterDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public UserCallcenterDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public UserCallcenterDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<BuscarUsuario> BuscarUsuarios
		{
			get
			{
				return this.GetTable<BuscarUsuario>();
			}
		}
		
		public System.Data.Linq.Table<TB_PROD_UsuarioCallcenter> TB_PROD_UsuarioCallcenters
		{
			get
			{
				return this.GetTable<TB_PROD_UsuarioCallcenter>();
			}
		}
		
		public System.Data.Linq.Table<NuevosCallCenters> NuevosCallCenters
		{
			get
			{
				return this.GetTable<NuevosCallCenters>();
			}
		}
		
		public System.Data.Linq.Table<Entidades> Entidades
		{
			get
			{
				return this.GetTable<Entidades>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TB_AUT_Usuario")]
	public partial class BuscarUsuario : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private string _nombre;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnnombreChanging(string value);
    partial void OnnombreChanged();
    #endregion
		
		public BuscarUsuario()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="USUA_NombreCompleto", Storage="_nombre", DbType="VarChar(250) NOT NULL", CanBeNull=false, IsPrimaryKey=true)]
		public string nombre
		{
			get
			{
				return this._nombre;
			}
			set
			{
				if ((this._nombre != value))
				{
					this.OnnombreChanging(value);
					this.SendPropertyChanging();
					this._nombre = value;
					this.SendPropertyChanged("nombre");
					this.OnnombreChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TB_PROD_UsuarioCallcenter")]
	public partial class TB_PROD_UsuarioCallcenter
	{
		
		private int _USCA_USUA_Codigo;
		
		private int _USCA_EVEN_Codigo;
		
		private System.DateTime _USCA_FechaCreacion;
		
		public TB_PROD_UsuarioCallcenter()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_USCA_USUA_Codigo", DbType="Int NOT NULL")]
		public int USCA_USUA_Codigo
		{
			get
			{
				return this._USCA_USUA_Codigo;
			}
			set
			{
				if ((this._USCA_USUA_Codigo != value))
				{
					this._USCA_USUA_Codigo = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_USCA_EVEN_Codigo", DbType="Int NOT NULL")]
		public int USCA_EVEN_Codigo
		{
			get
			{
				return this._USCA_EVEN_Codigo;
			}
			set
			{
				if ((this._USCA_EVEN_Codigo != value))
				{
					this._USCA_EVEN_Codigo = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_USCA_FechaCreacion", DbType="DateTime NOT NULL")]
		public System.DateTime USCA_FechaCreacion
		{
			get
			{
				return this._USCA_FechaCreacion;
			}
			set
			{
				if ((this._USCA_FechaCreacion != value))
				{
					this._USCA_FechaCreacion = value;
				}
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TB_PROD_UsuarioCallcenter")]
	public partial class NuevosCallCenters
	{
		
		private System.Guid _EVEN_Token;
		
		private List<Entidades> _filtros;
		
		public NuevosCallCenters()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="EVEN_Token", Storage="_EVEN_Token", DbType="UniqueIdentifier NOT NULL")]
		public System.Guid token
		{
			get
			{
				return this._EVEN_Token;
			}
			set
			{
				if ((this._EVEN_Token != value))
				{
					this._EVEN_Token = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="", Storage="_filtros", CanBeNull=false)]
		public List<Entidades> callcenters
		{
			get
			{
				return this._filtros;
			}
			set
			{
				if ((this._filtros != value))
				{
					this._filtros = value;
				}
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.TB_MAE_Entidad")]
	public partial class Entidades
	{
		
		private System.Guid _token;
		
		private string _nombre;
		
		public Entidades()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="ENTI_Token", Storage="_token", DbType="UniqueIdentifier NOT NULL")]
		public System.Guid token
		{
			get
			{
				return this._token;
			}
			set
			{
				if ((this._token != value))
				{
					this._token = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="ENTI_Identificador", Storage="_nombre", DbType="VarChar(200) NOT NULL", CanBeNull=false)]
		public string nombre
		{
			get
			{
				return this._nombre;
			}
			set
			{
				if ((this._nombre != value))
				{
					this._nombre = value;
				}
			}
		}
	}
}
#pragma warning restore 1591
