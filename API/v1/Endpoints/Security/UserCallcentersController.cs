﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace API.Endpoints.Security.Services.UserCallcenters
{
    /// <summary>
    /// Account API
    /// </summary>
    //[Gale.Security.Oauth.Jwt.Authorize]
    public class UserCallcentersController : Gale.REST.RestController
    {

        #region USERCALLCENTER
        /// <summary>
        /// Retrieve Target Event Information
        /// </summary>
        /// <param name="id">Event Token</param>
        /// <returns></returns>
        [HttpGet]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.OK)]
       // [Gale.Security.Oauth.Jwt.Authorize(Roles = API.WebApiConfig.RootRoles)]
        public IHttpActionResult Get(String id)
        {
            //------------------------------------------------------------------------------------------------------
            // GUARD EXCEPTIONS
            Gale.Exception.RestException.Guard(() => !id.isGuid(), "ID_INVALID_GUID", API.Errors.ResourceManager);
            //------------------------------------------------------------------------------------------------------
            return new Services.UserCallcenters.Get(id);
        }


        /// <summary>
        /// Created an userCallcenter in the system
        /// </summary>
        /// <param name="userCallcenter">UserCallcenter information</param>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.Created)]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.BadRequest)]
        //[Gale.Security.Oauth.Jwt.Authorize(Roles = API.WebApiConfig.RootRoles)]
        public IHttpActionResult Post([FromBody]Models.UserCallcenters.NuevosCallCenters userCallcenter)
        {
            string host = this.Request.Headers.Referrer.ToString();
            return new Services.UserCallcenters.Create(userCallcenter, host);
        }
        #endregion

    }
}